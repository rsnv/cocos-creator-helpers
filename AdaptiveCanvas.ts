const {ccclass, property, requireComponent} = cc._decorator;

export module rsnv  {
    @ccclass
    @requireComponent(cc.Canvas)
    export class AdaptiveCanvas extends cc.Component {
    
        @property canvasWidth : number = 1334;
        @property canvasHeight: number = 750;
    
        private _canvas: cc.Canvas = undefined;
    
        onEnable() {
            if(!this._canvas) {
                this._canvas = this.getComponent<cc.Canvas>(cc.Canvas);
            } 
            
            cc.view.resizeWithBrowserSize(true);
    
            cc.view.setResizeCallback(() => {
                this.onResize();
            });
    
            this.resizeCanvas();
        }
    
        onDisable() {
            cc.view.setResizeCallback(undefined);
        }
    
        onResize() {
            this.resizeCanvas();
        }

        public updateCanvas() {
            this.resizeCanvas();
        }

        private resizeCanvas() {
            if(!this._canvas) return;
    
            let size = cc.view.getCanvasSize();
            let ratio = size.width / size.height;
            let needRotate = ratio < this.canvasWidth / this.canvasHeight;
    
            this._canvas.fitWidth = needRotate;
            this._canvas.fitHeight = !needRotate;
        }
    }

};