const {ccclass, property} = cc._decorator;

export module rsnv  {
    @ccclass
    export class CoverNode extends cc.Component {
        @property (cc.Node) target : cc.Node = null;
        
        private _ratio: number = 0;
        
        onEnable() {
           this._ratio = this.node.width / this.node.height;

           cc.view.resizeWithBrowserSize(true);
    
            cc.view.setResizeCallback(() => {
                this.onResize();
            });
    
            this.resizeNode();
        }
    
        onDisable() {
            cc.view.setResizeCallback(undefined);
        }

        onResize() {
            this.resizeNode();
        }

        private resizeNode() {
            const size = this.target.getContentSize();
            const ratio = size.width / size.height;
            
            if(ratio < this._ratio) {
                this.lockHeight(size);
            } else {
                this.lockWidth(size);
            }
        }

        private lockWidth(size: cc.Size) {
            this.node.width = size.width;
            this.node.height = size.width / this._ratio;
        }

        private lockHeight(size: cc.Size) {
            this.node.height = size.height;
            this.node.width = size.height * this._ratio;
        }
    }
};