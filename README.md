# Hello there
---
> Hello there!  
> General Kenobi, you are a bold one. :)

Some simple helpers for cocos creator made by myself.

## Getting started

1. Learn [TypeScript](https://www.typescriptlang.org/) little a bit.
1. Download and instal [Cocos Creator 2.X](https://cocos2d-x.org/download).
1. ENJOY!


## What repo contains

* **AdaptiveCanvas.ts** - component for simple lock some sides, based on display ratio and orientation.
* **CoverNode.ts** - component for simulate [background-size: cover](https://developer.mozilla.org/en-US/docs/Web/CSS/background-size#Values) for cc.Node.


## Author

* **Alex Rusanov** - *Dis is me* - [rsnv](https://bitbucket.org/rsnv/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
